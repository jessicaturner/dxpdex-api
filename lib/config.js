"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getConfig = exports.setConfig = exports.addConfig = void 0;
const defaultConfig = {
  name: "DxpChain",
  coreToken: "DXP",
  addressPrefix: "DXP",
  expireInSecs: 15,
  expireInSecsProposal: 24 * 60 * 60,
  reviewInSecsDxpcore: 24 * 60 * 60,
  chainId: "71a4332ce31af65a9f8b9503053644166754fab70ff0b559fbed8be8d0dcb6e2"
};
let networks = [defaultConfig, {
  name: "TestNet",
  coreToken: "TEST",
  addressPrefix: "TEST",
  expireInSecs: 15,
  expireInSecsProposal: 24 * 60 * 60,
  reviewInSecsDxpcore: 24 * 60 * 60,
  chainId: "71a4332ce31af65a9f8b9503053644166754fab70ff0b559fbed8be8d0dcb6e2"
}],
    current = null;

const addConfig = config => networks.push({ ...defaultConfig,
  ...config
});

exports.addConfig = addConfig;

const setConfig = chainId => current = networks.find(net => net.chainId === chainId);

exports.setConfig = setConfig;

const getConfig = () => current;

exports.getConfig = getConfig;
